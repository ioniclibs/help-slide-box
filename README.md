#Ionic Help SlideBox#

#Install with Bower from Git
bower install https://bitbucket.org/ioniclibs/help-slide-box.git#0.0.1

# Add module dependency
angular.module("myModule", ['ionic', 'helpSlideBox']);

# Insert directive in your html
<help-slide-box></help-slide-box>

#Define a unique id
You can have how many slide boxes you want in your app, but you must have a unique id to make it show properly.

<help-slide-box id="myHelpSlideBox"></help-slide-box>


# Create your slide data

In html
<help-slide-box id="myHelpSlideBox" data="items"></help-slide-box>


Inside your controller

$scope.items = [
    {
        title: "Item title",
        text: "Card description"
        version: 1,
        image: "path/to/image" // optional
    }
];

# Define a callback function
If you wanna know every time a slide is changed you can pass a callback function to directive. The current slide index will be passed as a param.

<help-slide-box id="myHelpSlideBox" data="items" on-slide-changed="slideHasChanged(index)"></help-slide-box>