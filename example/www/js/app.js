// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
angular.module('starter', ['ionic', 'helpSlideBox'])

    .run(function($ionicPlatform) {
        $ionicPlatform.ready(function() {
            // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
            // for form inputs)
            if(window.cordova && window.cordova.plugins.Keyboard) {
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
            }
            if(window.StatusBar) {
                StatusBar.styleDefault();
            }
        });
    })

    .controller("MainCtrl", function($scope, $q, $interval){
        var deffer = $q.defer();
        $interval(function (){
            deffer.resolve([
                {
                    title: "Primeiro Slide",
                    text: "Texto explicativo da funcionalidade.",
                    version: 1
                },
                {
                    title: "Segundo Slide",
                    text: "Texto explicativo da funcionalidade.",
                    version: 1
                },
                {
                    title: "Terceiro Slide",
                    text: "Texto explicativo da funcionalidade.",
                    version: 1
                }
            ]);
        }, 1000);


        var ajuda = deffer.promise;

        $scope.ajuda = [];
        ajuda.then(function(resultado){
            angular.copy(resultado, $scope.ajuda);

        });

    });
