var module = angular.module('helpSlideBox', ['helpSlideBox.templates', 'helpSlideBox.constant']);
angular.module("helpSlideBox.templates", []);
angular.module('helpSlideBox.constant', [])
    .constant('HelpSlideBoxEvents', {
        SHOW_HELP: 'showHelp'
    });
module.directive("helpSlideBox", ["$rootScope", "$templateCache", "$log", "$ionicScrollDelegate", function($rootScope, $templateCache, $log, $ionicScrollDelegate, $compile) {


    return {
        restrict: "E",
        scope: {
            onSlideChanged: "&",
            data: "="
        },
        controller: function($scope, $element, $attrs, $transclude, $compile, $templateCache, $rootScope, HelpSlideBoxEvents){

            $scope.currentSlideIndex = 0;
            $scope.slides = [];

            var uid = $element.attr('id');



            $rootScope.$on(HelpSlideBoxEvents.SHOW_HELP, function(event, id){
                console.log(id , 'help');
                if (id == uid) {
                    $scope.showAll();
                }
            });


            if (uid === undefined){
                $log.warn("You must define an id attribute for each <help-slide-box> directive.");
                uid = "helpSlideBox";
            }

            var currentVersion = 0,
                lastVersion = window.localStorage[uid] || 0;

            $scope.$watchCollection('data', function (value){
                if ($scope.slides.length === 0) {
                    $scope.data = value;
                    createSlides(lastVersion);
                }
            });

            $scope.visible = false;

            function createSlides(lastVersion){

                console.log($scope.data, 'log data');
                if (!$scope.data || $scope.data.length === 0){
                    return;
                }

                angular.forEach($scope.data, function(item){
                    if (item.version > currentVersion){
                        currentVersion = item.version;
                    }
                });

                angular.copy([], $scope.slides);
                angular.forEach($scope.data, function(item){
                    if (item.version > lastVersion){
                        $scope.slides.push(item);
                    }
                });

                $scope.visible = lastVersion < currentVersion;

                var html = $templateCache.get("help-slide-box.tpl.html");

                html = $compile(html)($scope);

                $element.children().remove();

                $element.append(html);
            }

            $scope.updateSlideIndex = function(index){
                $scope.currentSlideIndex = index;

                if ($scope.onSlideChanged){
                    $scope.onSlideChanged({index: index});
                }
            };

            $scope.discard = function(){
                window.localStorage[uid] = currentVersion;
                $scope.visible = false;
            };

            $scope.showAll = function() {
                window.localStorage[uid] = 0;
                createSlides(0);
                $scope.visible = true;
            };

            $scope.disableVerticalScrolling = function(){
                var scrollPos = $ionicScrollDelegate.getScrollPosition().top;
                $ionicScrollDelegate.scrollTo(0, scrollPos, false);
            };
        }
    };
}]);
angular.module("helpSlideBox.templates").run(["$templateCache", function($templateCache) {$templateCache.put("help-slide-box.tpl.html","<div class=\"help-slide-box\" ng-show=\"visible\" on-drag-left=\"disableVerticalScrolling()\" on-drag-right=\"disableVerticalScrolling()\">\r\n    <div class=\"item row-no-padding\">\r\n        <ion-slide-box on-slide-changed=\"updateSlideIndex($index)\" show-pager=\"false\">\r\n            <ion-slide ng-repeat=\"slide in slides\">\r\n                <div class=\"item item-text-wrap \">\r\n                    <h2>{{slide.title}}</h2>\r\n                    <img ng-src=\"{{slide.image}}\" width=\"100%\" ng-if=\"slide.image\" />\r\n                    <p>{{slide.text}}</p>\r\n                </div>\r\n            </ion-slide>\r\n        </ion-slide-box>\r\n    </div>\r\n\r\n    <div class=\"pager item row-no-padding\" ng-if=\"slides.length > 1\">\r\n        <div>\r\n            <span ng-repeat=\"slide in slides\" ng-class=\"{\'active\': $index === currentSlideIndex}\"></span>\r\n        </div>\r\n    </div>\r\n\r\n    <div class=\"item footer\" ng-click=\"discard()\">\r\n        <button class=\"button button-small button-clear\">\r\n            Ok, entendi\r\n        </button>\r\n    </div>\r\n </div>");}]);